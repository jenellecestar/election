<?php
  include "header.php"
?>

<div class="container">
  <div class="columns">
    <div class="column is-4 is-offset-4">

      <h1 class="vote header"> Step 2: Select your district</h1>

      <form action="select-candidate.php" method="">

        <div class="select">
          <select name="">
            <option value="">Ajax</option>
            <option value="">Algoma--Manitoulin</option>
            <option value="">Aurora--Oak Ridges--Richmond Hill</option>
            <option value="">Barrie--Innisfil</option>
          </select>
        </div>

        <button class="button is-link">Submit</button>

      </form>

    </div>  <!-- col-6 -->
  </div> <!-- columns -->
</div>
