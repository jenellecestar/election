<?php
  include "header.php"
?>

<div class="container">
  <div class="columns">
    <div class="column is-4 is-offset-4">

      <h1 class="vote header"> Step 1: Enter Voter Information</h1>

      <form action="select-district.php" method="">
        <div class="field">
          <label class="label">First Name</label>
          <div class="control">
            <input class="input" type="text" name="">
          </div>
        </div>
        <div class="field">
          <label class="label">Last Name</label>
          <div class="control">
            <input class="input" type="text" name="">
          </div>
        </div>
        <div class="field">
          <label class="label">Postal Code</label>
          <div class="control">
            <input class="input" type="text" maxlength="6" name="">
          </div>
        </div>
        <div class="field">
          <label class="label">Access Code</label>
          <div class="control">
            <input class="input" type="text" name="">
          </div>
        </div>
        <div class="field">
          <div class="control">
            <button class="button is-link">Submit</button>
          </div>
        </div>
      </form>

    </div>  <!-- col-6 -->
  </div> <!-- columns -->
</div>
