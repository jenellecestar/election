<?php
  include "header.php";
?>


<div class="container">
  <div class="columns">
      <div class="column is-12 has-text-centered">
        <h1>The election will be held on: June 15, 2018 at 9:00am.</h1>

        <a class="button is-link" href="get-key.php">Get Access Key</a>
        <a class="button is-info" href="go-vote.php">Go Vote</a>
      </div>
  </div>
</div>


<?php
  include "footer.php";
?>
