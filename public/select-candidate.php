<?php
  include "header.php"
?>

<div class="container">
  <div class="columns">
    <div class="column is-4 is-offset-4">
      <h1 class="vote header"> Step 3: Select your candidate</h1>
      <h1 class="vote header"> Your district is: University--Rosedale</h1>

      <form action="vote-confirm.php" method="">
        <div class="control">
          <label class="radio">
            <input type="radio" name="person">
            PC: Sam Smith
          </label>
          <br>
          <label class="radio">
            <input type="radio" name="person">
            Liberal: Peter Wilson
          </label>
          <br>
          <label class="radio">
            <input type="radio" name="person">
            NDP: Justin Bieber
          </label>
          <br>
          <label class="radio">
            <input type="radio" name="person">
            Green: Michelle Pincus
          </label>
        </div>

        <button class="button is-link">Submit</button>

      </form>
    </div>  <!-- col-6 -->
  </div> <!-- columns -->
</div>
