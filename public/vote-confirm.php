<?php
  include "header.php"
?>

<div class="container">
  <div class="columns">
    <div class="column is-4 is-offset-4">
      <h1 class="vote header"> Your vote was registered!</h1>

      <a href="index.php" class="button is-link">Go Home</a>

      </form>
    </div>  <!-- col-6 -->
  </div> <!-- columns -->
</div>
