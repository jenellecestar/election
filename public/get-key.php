<?php
  include "header.php"
?>

<div class="container">
  <div class="columns">
    <div class="column is-4 is-offset-4">

      <h1> Get Your Voting Access Key </h1>

      <form action="next.php" method="POST">
        <div class="field">
          <label class="label">First Name</label>
          <div class="control">
            <input class="input" type="text">
          </div>
        </div>
        <div class="field">
          <label class="label">Last Name</label>
          <div class="control">
            <input class="input" type="text">
          </div>
        </div>
        <div class="field">
          <label class="label">Postal Code</label>
          <div class="control">
            <input class="input" type="text" maxlength="6">
          </div>
        </div>
        <div class="field">
          <div class="control">
            <button class="button is-link">Submit</button>
          </div>
        </div>
      </form>
    </div>  <!-- div class= column is-4 -->
  </div> <!-- div class = columns -->
</div> <!-- container -->
